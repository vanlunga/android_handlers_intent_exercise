package com.vlitstech.www.activityswitchassignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button cat1 = (Button) findViewById(R.id.cat1Button);
        Button cat2 = (Button) findViewById(R.id.cat2Button);
        Button cat3 = (Button) findViewById(R.id.cat3Button);
        Button cat4 = (Button) findViewById(R.id.cat4Button);

        cat1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(),Category1Activity.class);
                startActivity(i);
            }
        });

        cat2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(),Category2Activity.class);
                startActivity(i);
            }
        });

        cat3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(),Category3Activity.class);
                startActivity(i);
            }
        });

        cat4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(getApplicationContext(),Category4Activity.class);
                startActivity(i);
            }
        });


    }
}
